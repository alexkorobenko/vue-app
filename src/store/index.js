import Vue from 'vue';
import VueFire from 'vuefire';
import Firebase from 'firebase';
import Vuex from 'vuex';

Vue.use(VueFire);
Vue.use(Vuex);

let config = {
    apiKey: "AIzaSyCeewQ1yPqt9CNPv6Qcfuke2b26Uvmj3gc",
    authDomain: "vue-js-af874.firebaseapp.com",
    databaseURL: "https://vue-js-af874.firebaseio.com",
    projectId: "vue-js-af874",
    storageBucket: "vue-js-af874.appspot.com",
    messagingSenderId: "373846640428"
};

window.app = Firebase.initializeApp(config);
window.auth = app.auth();
window.storage = app.storage();
window.db = app.database();

let dbTablesName = {
    categories: 'categories',
    products: 'products'
};

const store = new Vuex.Store({

    state: {
        categories: {},
        products: {}
    },

    actions: {

        LOAD_CATEGORIES: function ({commit}) {
            let categoriesRef = db.ref(dbTablesName.categories).orderByKey();

            return new Promise(function (resolve) {
                categoriesRef.once('value').then(function(snapshot) {
                    commit('SET_STATE_CATEGORIES', snapshot);
                    resolve();
                });
            });
        },

        LOAD_PRODUCTS: function ({commit}) {
            let productsRef = db.ref(dbTablesName.products).orderByKey();

            return new Promise(function (resolve) {
                productsRef.once('value').then(function(snapshot) {
                    commit('SET_STATE_PRODUCTS', snapshot);
                    resolve();
                });
            });
        },

        GET_CATEGORY_BY_ID: function ({commit}, id) {
            return new Promise(function (resolve, reject) {
                let response = {},
                    category = {};

                if ( id == undefined) {
                    response.error = 'Что то пошло не так';
                    reject(response);
                }

                let categoryRef = db.ref(dbTablesName.categories + '/' + id);

                categoryRef.once('value').then(function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        let key = childSnapshot.key,
                            value = childSnapshot.val();

                        category[key] = value;
                    });

                    if ( Object.keys(category).length ) {
                        response.category = category;
                        response.categoryRef = categoryRef;
                        resolve(response);
                    } else {
                        response.error = 'Что то пошло не так';
                        reject(response);
                    }
                });
            });
        },

        GET_PRODUCT_BY_ID: function ({commit}, id) {
            return new Promise(function (resolve, reject) {
                let response = {},
                    product = {};

                if ( id == undefined) {
                    response.error = 'Что то пошло не так';
                    reject(response);
                }

                let productRef = db.ref(dbTablesName.products + '/' + id);

                productRef.once('value').then(function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        let key = childSnapshot.key,
                            value = childSnapshot.val();

                        product[key] = value;
                    });

                    if ( Object.keys(product).length ) {
                        response.product = product;
                        response.productRef = productRef;
                        resolve(response);
                    } else {
                        response.error = 'Что то пошло не так';
                        reject(response);
                    }
                });
            });
        },

        SET_CATEGORY: function ({commit}, [category, categoryRef]) {
            return new Promise(function (resolve, reject) {
                let response = {};

                if ( category.name == undefined || category.name == '' ) {
                    response.error = 'Введите имя категории';
                    reject(response);
                }

                categoryRef.set(category).then(function() {
                    response.success = 'Категория обновлена';
                    resolve(response);
                });
            });
        },

        SET_PRODUCT: function ({commit}, [product, productRef]) {
            return new Promise(function (resolve, reject) {
                let response = {};

                if ( product.name == undefined || product.name == '' ) {
                    response.error = 'Введите имя продукта';
                    reject(response);
                }

                productRef.set(product).then(function() {
                    response.success = 'Продукт обновлен';
                    resolve(response);
                });
            });
        },

        ADD_CATEGORY: function ({commit}, category) {
            return new Promise(function (resolve, reject) {
                let categoriesRef = db.ref(dbTablesName.categories),
                    newCategoriesRef = categoriesRef.push(),
                    response = {};

                if ( category.name == '' ) {
                    response.error = 'Введите имя категории';
                    reject(response);
                }

                newCategoriesRef.set(category).then(function() {
                    response.success = 'Категория добавлена';
                    resolve(response);
                });
            });
        },

        ADD_PRODUCT: function ({commit}, product) {
            return new Promise(function (resolve, reject) {
                let productsRef = db.ref(dbTablesName.products),
                    newProductsRef = productsRef.push(),
                    response = {};

                if ( product.categoryId == '' || product.name == '' ) {
                    response.error = 'Заполните обезательные поля';
                    reject(response);
                }

                newProductsRef.set(product).then(function() {
                    response.success = 'Продукт добавлен';
                    resolve(response);
                });
            });
        },

        REMOVE_CATEGORY: function ({commit}, id) {
            return new Promise(function (resolve, reject) {
                let productsRef = db.ref(dbTablesName.products),
                    response = {},
                    status = 1;

                productsRef.once('value').then(function(snapshot) {
                    snapshot.forEach(function(childSnapshot) {
                        if ( childSnapshot.val().categoryId == id ) {
                            return status = 0;
                        }
                    });

                    if ( status ) {
                        let categoryRef = db.ref(dbTablesName.categories + '/' + id);

                        categoryRef.remove().then(function () {
                            response.success = 'Категория удалена';
                            commit('REMOVE_STATE_CATEGORY', id);
                            resolve(response);
                        });
                    } else {
                        response.error = 'Нельзя удалить категорию к которой привязаны товары';
                        reject(response);
                    }
                });
            });
        },

        REMOVE_PRODUCT: function ({commit}, id) {
            return new Promise(function (resolve, reject) {
                let response = {},
                    status = 1;

                if ( id == undefined) {
                    response.error = 'Что то пошло не так';
                    reject(response);
                }

                let productRef = db.ref(dbTablesName.products + '/' + id);

                productRef.remove().then(function () {
                    response.success = 'Продукт удален';
                    commit('REMOVE_STATE_PRODUCT', id);
                    resolve(response);
                });
            });
        }

    },

    mutations: {

        SET_STATE_CATEGORIES: function (state, snapshot) {
            state.categories = {};

            snapshot.forEach(function(childSnapshot) {
                let key = childSnapshot.key,
                    value = childSnapshot.val();

                state.categories[key] = {};
                state.categories[key] = value;
                state.categories[key].id = key;
            });
        },

        SET_STATE_PRODUCTS: function (state, snapshot) {
            state.products = {};

            snapshot.forEach(function(childSnapshot) {
                let key = childSnapshot.key,
                    value = childSnapshot.val();

                state.products[key] = {};
                state.products[key] = value;
                state.products[key].id = key;
            });
        },

        REMOVE_STATE_CATEGORY: function (state, id) {
            let categories = state.categories;

            for (let key in categories) {
                if (categories[key].id == id) {
                    delete categories[key];
                }
            }

            state.categories = {};
            state.categories = categories;
        },

        REMOVE_STATE_PRODUCT: function (state, id) {
            let products = state.products;

            for (let key in products) {
                if (products[key].id == id) {
                    delete products[key];
                }
            }

            state.products = {};
            state.products = products;
        }

    },

    getters: {

        categories: function (state) {
            return state.categories;
        },

        products: function (state) {
            return state.products;
        }

    }

});

export default store;