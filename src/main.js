// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue';
import VueRouter from 'vue-router';
import VeeValidate from 'vee-validate';
import _ from 'lodash';

import CxltToastr from 'cxlt-vue2-toastr';
import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css';

import store from './store/index.js';

import App from './app';
import Login from './components/auth/login';
import Registration from './components/auth/registration';
import EmailVerification from './components/auth/email-verification';
import Home from './components/home/index';
import Profile from './components/profile/index';

import Categories from './components/category/index';
import EditCategory from './components/category/edit';
import AddCategory from './components/category/add';

import Products from './components/product/index';
import EditProduct from './components/product/edit';
import AddProduct from './components/product/add';

import './sass/main.scss';

Vue.config.productionTip = false;
Vue.use(VueRouter);
Vue.use(VeeValidate);

let toastrConfigs = {
    position: 'top right',
    showDuration: 1000,
    timeOut: 3000,
    closeButton: false
};

Vue.use(CxltToastr, toastrConfigs);

let router = new VueRouter({
    linkActiveClass: 'active',
    transitionOnLoad: false,
    mode: 'hash',
    base: window.location.href,
    // mode: 'history',
    root: '/',
    routes: [
        { path: '/', name: 'home', component: Home },
        { path: '/login', name: 'login', component: Login },
        { path: '/registration', name: 'registration', component: Registration },
        { path: '/email-verification', name: 'emailVerification', component: EmailVerification },
        { path: '/profile', name: 'profile', component: Profile },
        { path: '*', name: 'error', component: Home },

        { path: '/categories', name: 'categories', component: Categories },
        { path: '/edit-category/:id', name: 'edit-category', component: EditCategory },
        { path: '/add-category/', name: 'add-category', component: AddCategory },

        { path: '/products', name: 'products', component: Products },
        { path: '/edit-product/:id', name: 'edit-product', component: EditProduct },
        { path: '/add-product/', name: 'add-product', component: AddProduct }
    ]
});

new Vue({

    el: '#app',
    router: router,
    store: store,

    data: function() {
        return {}
    },

    mounted: function () {
        let self = this;

        self.showPageLoader();

        self.detectUser().then(function (user) {
            self.user = user;
            self.hidePageLoader();
        });
    },

    methods: {

        detectUser: function () {
            let self = this;

            return new Promise(function (resolve) {
                auth.onAuthStateChanged(function (user) {
                    if (user) {

                        if (!user.emailVerified) {
                            if (self.$route.name == 'email-verification') {
                                resolve(user);
                            } else {
                                self.$router.push( {name: 'emailVerification'} );
                            }
                        }

                        if ( self.$route.name == 'login' || self.$route.name == 'registration' || self.$route.name == 'email-verification' ) {
                            self.$router.push( {name: 'home'} );
                        } else {
                            document.getElementById('page-sidebar').classList.add('active');
                            resolve(user);
                        }

                    } else {

                        if ( self.$route.name == 'login' || self.$route.name == 'registration' ) {
                            document.getElementById('page-sidebar').classList.remove('active');
                            resolve();
                        } else {
                            self.$router.push( {name: 'login'} );
                        }

                    }
                });
            });
        },

        login: function (email, password) {
            return new Promise(function (resolve, reject) {
                auth.signInWithEmailAndPassword(email, password).catch(function (error) {
                    reject(error);
                });
            });
        },

        logout: function () {
            return new Promise(function (resolve, reject) {
                auth.signOut().catch(function (error) {
                    reject(error);
                });
            });

        },

        registration: function (email, password) {
            return new Promise(function (resolve, reject) {
                auth.createUserWithEmailAndPassword(email, password).catch(function (error) {
                    reject(error);
                });
            });
        },

        showPageLoader: function () {
            document.getElementById('page-loading').classList.remove('hide');
        },

        hidePageLoader: function () {
            setTimeout(function () {
                document.getElementById('page-loading').classList.add('hide');
            }, 300);
        }

    },

    template: '<App/>',
    components: { App }

});
